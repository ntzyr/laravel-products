<?php


use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateVariablesTable extends Migration
{
    public function up()
    {
        Schema::create('variables', function (Blueprint $table) {
            $table->unsignedBigInteger('variable_id');
            $table->unsignedBigInteger('product_id');

            $table->foreign('variable_id')
                ->references('id')->on('variable_products');
            $table->foreign('product_id')
                ->references('id')->on('simple_products');
        });
    }

    public function down()
    {
        Schema::dropIfExists('variables');
    }
}
