<?php


use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateGroupedTable extends Migration
{
    public function up()
    {
        Schema::create('grouped', function (Blueprint $table) {
            $table->unsignedBigInteger('group_id');
            $table->string('model_type');
            $table->unsignedBigInteger('model_id');

            $table->foreign('group_id')
                ->references('id')->on('group_products');
        });
    }

    public function down()
    {
        Schema::dropIfExists('variables');
    }
}
