<?php


use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateModelHasProductTable extends Migration
{
    public function up()
    {
        Schema::create('model_has_product', function (Blueprint $table) {
            $table->string('product_type');
            $table->unsignedBigInteger('product_id');
            $table->string('model_type');
            $table->unsignedBigInteger('model_id');
        });
    }

    public function down()
    {
        Schema::dropIfExists('model_has_product');
    }
}
