<?php


use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateVariableProductsTable extends Migration
{
    public function up()
    {
        Schema::create('variable_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sku')->unique();
            $table->string('slug')->unique()->nullable();
            $table->string('name')->nullable();
            $table->text('description')->nullable();
            $table->double('price')->nullable();
            $table->double('sale_price')->nullable();
            $table->bigInteger('views')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('variable_products');
    }
}
