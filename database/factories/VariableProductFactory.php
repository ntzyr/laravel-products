<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Carbon\Carbon;
use Faker\Generator as Faker;
use Illuminate\Support\Str;
use Ntzyr\LaravelProducts\Models\VariableProduct;

$factory->define(VariableProduct::class, function (Faker $faker) {
    $productName = $faker->unique()->words(random_int(1,4), true);

    return [
        'sku' => Str::random(10),
        'slug' => Str::slug($productName),
        'name' => Str::title($productName),
        'description' => $faker->realText(),
        'price' => $faker->randomFloat(2, 0, 30000),
        'sale_price' => $faker->randomFloat(2, 0, 20000),
        'views' => $faker->numberBetween(0, 300),
        'created_at' => $faker->dateTimeBetween('-' . rand(10, 20) . '5 days', 'now')->format('Y-m-d H:i:s')
    ];
});