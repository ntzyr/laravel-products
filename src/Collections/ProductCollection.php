<?php


namespace Ntzyr\LaravelProducts\Collections;


use Illuminate\Database\Eloquent\Collection;
use Ntzyr\LaravelProducts\Models\AbstractProduct;

class ProductCollection extends Collection
{
    public function __construct($items = [])
    {
        parent::__construct($items);
    }

    public function merge($items)
    {
        $dictionary = $this->getDictionary();

        foreach ($items as $item) {
            $dictionary[$item->sku] = $item;
        }

        return new static(array_values($dictionary));
    }

    public static function allProducts()
    {
        $types = AbstractProduct::productsTypes();
        $items = new self;

        foreach ($types as $type) {
            $items = $items->merge($type::all());
        }

        return new self($items);
    }
}
