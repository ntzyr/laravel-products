<?php


namespace Ntzyr\LaravelProducts\Traits;


use Illuminate\Support\Facades\DB;
use Ntzyr\LaravelProducts\Collections\ProductCollection;
use Ntzyr\LaravelProducts\Models\AbstractProduct;
use Exception;

trait HasProducts
{
    /**
     * @param $type
     * @return mixed
     */
    public function relationsWithProductType($type)
    {
        return $this->belongsToMany(
            $type,
            'model_has_product',
            'model_id',
            'product_id'
        )->where('product_type', $type);
    }

    /**
     * @param $product
     * @return bool
     */
    public function hasProduct($product)
    {
        $relation = $this->relationsWithProductType(get_class($product))->where('product_id', $product->id);

        return $relation->count() > 0 ? true : false;
    }

    /**
     * @return ProductCollection
     */
    public function products()
    {
        $items = new ProductCollection;

        foreach (AbstractProduct::productsTypes() as $type) {
            $items = $items->merge($this->relationsWithProductType($type)->get());
        }

        return $items;
    }

    /**
     * @param $product
     * @throws Exception
     */
    public function checkProduct($product)
    {
        if(!in_array(get_class($product), AbstractProduct::productsTypes())) {
            throw new Exception('Given model is not product');
        }
    }

    /**
     * @param $product
     * @return mixed
     * @throws Exception
     */
    public function attachProduct($product)
    {
        $this->checkProduct($product);

        $relation = [
            'product_id' => $product->id,
            'product_type' => get_class($product),
            'model_id' => $this->id,
            'model_type' => self::class
        ];

        DB::table('model_has_product')
            ->insert($relation);

        return $product;
    }

    /**
     * @param $product
     * @return bool
     * @throws Exception
     */
    public function detachProduct($product)
    {
        $this->checkProduct($product);

        $attributes = [
            ['product_id', $product->id],
            ['product_type', get_class($product)],
            ['model_id', $this->id],
            ['model_type', self::class]
        ];

        if($this->hasProduct($product)) {
            DB::table('model_has_product')
                ->where($attributes)
                ->delete();
        } else {
            throw new Exception('This model has not any relations with this product');
        }

        return true;
    }
}