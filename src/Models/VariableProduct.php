<?php


namespace Ntzyr\LaravelProducts\Models;


class VariableProduct extends AbstractProduct
{
    protected $table = 'variable_products';

    public function variables()
    {
        return $this->belongsToMany(
            SimpleProduct::class,
            'variables',
            'variable_id',
            'product_id'
        );
    }
}
