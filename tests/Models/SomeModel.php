<?php


namespace Ntzyr\LaravelProducts\Tests\Models;


use Illuminate\Database\Eloquent\Model;
use Ntzyr\LaravelProducts\Traits\HasProducts;

class SomeModel extends Model
{
    use HasProducts;
}