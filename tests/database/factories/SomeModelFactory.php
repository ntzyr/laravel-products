<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Ntzyr\LaravelProducts\Tests\Models\SomeModel;

$factory->define(SomeModel::class, function (Faker $faker) {

    return [
        'name' => $faker->unique()->words(random_int(1,4), true)
    ];
});