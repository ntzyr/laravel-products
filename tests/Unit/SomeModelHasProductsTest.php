<?php


namespace Ntzyr\LaravelProducts\Tests\Unit;


use Ntzyr\LaravelProducts\Models\AbstractProduct;
use Ntzyr\LaravelProducts\Models\SimpleProduct;
use Ntzyr\LaravelProducts\Models\VariableProduct;
use Ntzyr\LaravelProducts\Tests\Models\SomeModel;
use Ntzyr\LaravelProducts\Tests\TestCase;

class SomeModelHasProductsTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        $this->loadMigrationsFrom(
            __DIR__ . '/../../database/migrations'
        );
        $this->withFactories(
            __DIR__ . '/../../database/factories'
        );

        factory(SimpleProduct::class, 10)->create();
        factory(VariableProduct::class, 10)->create();
    }

    public function testSomeModelHasProducts()
    {
        $simpleProduct = SimpleProduct::inRandomOrder()->take(1)->get()->first();
        $variableProduct = VariableProduct::inRandomOrder()->take(1)->get()->first();

        $someModel = factory(SomeModel::class)->create();

        $someModel->attachProduct($simpleProduct);
        $someModel->attachProduct($variableProduct);

        dd([
            $someModel->products()
        ]);
    }

    public function testSomeModelAttachProduct()
    {
        $simpleProduct = SimpleProduct::inRandomOrder()->take(1)->get()->first();

        $someModel = factory(SomeModel::class)->create();

        dd([
            $someModel->attachProduct($simpleProduct)
        ]);
    }

    public function testSomeModelHasProductsHasProductRelation()
    {
        $simpleProduct = SimpleProduct::inRandomOrder()->take(1)->get()->first();
        $secondSimpleProduct = SimpleProduct::inRandomOrder()->take(1)->get()->first();
        $anotherSimpleProduct = SimpleProduct::inRandomOrder()->take(1)->get()->first();
        $variableProduct = VariableProduct::inRandomOrder()->take(1)->get()->first();

        $someModel = factory(SomeModel::class)->create();

        $someModel->attachProduct($simpleProduct);
        $someModel->attachProduct($secondSimpleProduct);
        $someModel->attachProduct($variableProduct);

        dd([
            $someModel->hasProduct($simpleProduct),
            $someModel->hasProduct($anotherSimpleProduct)
        ]);
    }

    public function testSomeModelDetachProduct()
    {
        $simpleProduct = SimpleProduct::inRandomOrder()->take(1)->get()->first();
        $secondSimpleProduct = SimpleProduct::inRandomOrder()->take(1)->get()->first();

        $someModel = factory(SomeModel::class)->create();

        $someModel->attachProduct($simpleProduct);
        $someModel->attachProduct($secondSimpleProduct);

        dd([
            $someModel->detachProduct($simpleProduct),
            $simpleProduct->name,
            $secondSimpleProduct->name,
            $someModel->products()->pluck('name')
        ]);
    }

    public function testSomeModelDetachProductWrong()
    {
        $simpleProduct = SimpleProduct::inRandomOrder()->take(1)->get()->first();
        $secondSimpleProduct = SimpleProduct::inRandomOrder()->take(1)->get()->first();

        $someModel = factory(SomeModel::class)->create();
        $secondSomeModel = factory(SomeModel::class)->create();

        $someModel->attachProduct($simpleProduct);
        $someModel->attachProduct($secondSimpleProduct);

        dd([
            $someModel->detachProduct($secondSomeModel),
            $simpleProduct->name,
            $secondSimpleProduct->name,
            $someModel->products()->pluck('name')
        ]);
    }
}