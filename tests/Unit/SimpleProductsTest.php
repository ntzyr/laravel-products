<?php


namespace Ntzyr\LaravelProducts\Tests\Unit;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Ntzyr\LaravelProducts\Models\SimpleProduct;
use Ntzyr\LaravelProducts\Models\VariableProduct;
use Ntzyr\LaravelProducts\Tests\Models\SomeModel;
use Ntzyr\LaravelProducts\Tests\TestCase;

class SimpleProductsTest extends TestCase
{
    public function testSimpleProductCreate()
    {
        $data = [
            'name' => Str::random(10),
            'sku' => Str::random(10),
        ];

        $simple = SimpleProduct::create($data);

        dd(
            $simple
        );
    }

    public function testSimpleProductAttachTo()
    {
        $simple = factory(SimpleProduct::class)->create();
        $someModel = factory(SomeModel::class)->create();

        dd([
            $simple->attachTo($someModel),
            $someModel->hasProduct($simple),
            $simple->hasModel($someModel),
        ]);
    }

    public function testSimpleProductDetachFrom()
    {
        $simple = factory(SimpleProduct::class)->create();
        $someModel = factory(SomeModel::class)->create();

        $simple->attachTo($someModel);

        $this->assertTrue($simple->detachFrom($someModel));
        $this->assertFalse($someModel->hasProduct($simple));
        $this->assertFalse($simple->hasModel($someModel));
    }
}
