<?php


namespace Ntzyr\LaravelProducts\Tests\Unit;


use Ntzyr\LaravelProducts\Collections\ProductCollection;
use Ntzyr\LaravelProducts\Models\AbstractProduct;
use Ntzyr\LaravelProducts\Models\SimpleProduct;
use Ntzyr\LaravelProducts\Models\VariableProduct;
use Ntzyr\LaravelProducts\Tests\TestCase;

class ProductCollectionTest extends TestCase
{
    private $simpleProducts;
    private $variableProducts;

    protected function setUp(): void
    {
        parent::setUp();

        AbstractProduct::productsTypes([
            'simple_products' => SimpleProduct::class,
            'variable_products' => VariableProduct::class
        ]);

        $this->variableProducts = factory(VariableProduct::class, 3)->create();
        $this->simpleProducts = factory(SimpleProduct::class, 10)->create();
    }

    public function testProductCollectionSingleModel()
    {
        dd(
            new ProductCollection(SimpleProduct::all())
        );
    }

    public function testProductCollectionMultipleModel()
    {
        dd([
            ProductCollection::allProducts()->toArray()
        ]);
    }
}
