<?php


namespace Ntzyr\LaravelProducts\Tests\Unit;


use Illuminate\Support\Str;
use Ntzyr\LaravelProducts\Models\SimpleProduct;
use Ntzyr\LaravelProducts\Models\VariableProduct;
use Ntzyr\LaravelProducts\Tests\TestCase;

class VariableProductsTest extends TestCase
{
    public function testVariableProductCreate()
    {
        $data = [
            'name' => Str::random(10),
            'sku' => Str::random(10)
        ];

        $variable = VariableProduct::create($data);

        dd(
            $variable
        );
    }

    public function testVariableProductRelation()
    {
        $variable_data = [
            'name' => Str::random(10)
        ];

        $simple_data = [
            'name' => Str::random(10)
        ];

        $variable = VariableProduct::create($variable_data);
        $simple = SimpleProduct::create($simple_data);

        $variable->variables()->attach($simple->id);

        dd([
            $variable->toArray(),
            $variable->variables->toArray()
        ]);
    }
}
